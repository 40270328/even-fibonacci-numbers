﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Even_Fibonacci_Numbers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
         
            uint fibonacciNum = 0, f1 = 0, f2 = 1, answer = 0;

            fibonacciNum = f1 + f2;

            while(fibonacciNum < 4000000)
            {
                fibonacciNum = f1 + f2;
                f1 = f2;
                f2 = fibonacciNum;

                if (fibonacciNum % 2 == 0)
                {
                    answer = answer + fibonacciNum;
                }
            }

            lblAnswer.Content = "Answer = " + answer;
        }
    }
}
